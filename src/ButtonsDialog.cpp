#include "ButtonsDialog.h"
#include "ui_ButtonsDialog.h"

ButtonsDialog::ButtonsDialog(QWidget* parent)
    : QDialog(parent), ui(new Ui::ButtonsDialog)
{
    ui->setupUi(this);
}

ButtonsDialog::~ButtonsDialog()
{
    delete ui;
}

QString ButtonsDialog::GetButtons()
{
    return ui->textEdit->toPlainText();
}

void ButtonsDialog::SetButtons(QString btns)
{
    ui->textEdit->setText(btns);
}

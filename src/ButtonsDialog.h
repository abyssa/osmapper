#ifndef BUTTONSDIALOG_H
#define BUTTONSDIALOG_H

#include <QDialog>

namespace Ui
{
    class ButtonsDialog;
}

class ButtonsDialog : public QDialog
{
Q_OBJECT

public:
    explicit ButtonsDialog(QWidget* parent = 0);
    ~ButtonsDialog();

private:
    Ui::ButtonsDialog* ui;

public:
    QString GetButtons();
    void SetButtons(QString btns);
};

#endif // BUTTONSDIALOG_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QMainWindow>
#include <QGeoSatelliteInfoSource>
#include <QGeoPositionInfoSource>
#include <QGeoSatelliteInfo>
#include <QGeoPositionInfo>
#include <QSignalMapper>
#include <QStringList>
#include <QCloseEvent>

QTM_USE_NAMESPACE

namespace Ui
{
class MainWindow;
}

class PosInfo
{
public:
    PosInfo( const double  lat, const double lon, const double alt,
             const QDateTime tim);
    double latitude;
    double longitude;
    double altitude;
    QString DateTime;
};

class WayInfo
{
public:
    WayInfo(PosInfo* Pos, const QString WpT);
    PosInfo* Position;
    QString WayPointType;
};

#define OSMVersion = "4.0"

class MainWindow : public QMainWindow
{
    Q_OBJECT

protected:
    void closeEvent ( QCloseEvent* event );

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow* ui;
    QGeoSatelliteInfoSource* m_SatelliteInfo;
    int usedSatellites;
    int visibleSatellites;
    QGeoPositionInfoSource* m_location;
    QList<PosInfo*> track;
    QList<WayInfo*> Waypoints;
    QStringList Buttons;
    QSignalMapper* signalMapper;
    int UpdInt;
    bool running;
    double minLat;
    double maxLat;
    double minLon;
    double maxLon;
    QString LastPath;
    int Bpl;

private slots:
    void on_actionButtons_pr_line_triggered();
    void on_actionSample_rate_triggered();
    void on_actionNew_button_triggered();
    void pushButton_clicked(int);
    void on_actionAbout_triggered();
    void on_actionStart_mapping_triggered();
    void satellitesInUse(QList<QGeoSatelliteInfo> slt);
    void satellitesInView(QList<QGeoSatelliteInfo> slt);
    void positionUpdated(QGeoPositionInfo);
    void BuildButtons();
    void CommitButtons();
    void SetUpdateInterval();

private:
    void setStatusLine();
    void ExportTrack();
    void ReadConfig();
    void WriteConfig();
    void AddButton(const QString Titel, const int Id, const int x, const int y);

};

#endif // MAINWINDOW_H

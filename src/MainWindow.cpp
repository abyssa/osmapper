#include <QPushButton>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include "ButtonsDialog.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

QString FormatDateTime(QDateTime dt)
{
    return dt.toString("yyyy'-'MM'-'dd'T'hh':'mm':'ss'.'zzz");
}

WayInfo::WayInfo(PosInfo* Pos, const QString WpT)
{
    Position = Pos;
    WayPointType = WpT;
}

PosInfo::PosInfo( const double  lat, const double lon, const double alt,
                  const QDateTime tim)
{
    latitude = lat;
    longitude = lon;
    altitude = alt;
    DateTime = FormatDateTime(tim);
}

void MainWindow::AddButton(const QString Titel, const int Id, const int x,
                           const int y)
{
    QPushButton* pbOther =new QPushButton(Titel);
    signalMapper->setMapping(pbOther, Id);
    connect(pbOther, SIGNAL(clicked()), signalMapper, SLOT(map()));
    ui->gridLayout->addWidget(pbOther,x,y);
}

void MainWindow::BuildButtons()
{
    QLayoutItem* child;
    while ((child = ui->gridLayout->takeAt(0)) != 0)
    {
        delete child->widget();
        delete child;
    }

    int i;
    for (i=0; i<Buttons.count(); i++)
        AddButton(Buttons[i],i,i / Bpl,i % Bpl);

    AddButton("Other",9999,i / Bpl,i % Bpl);
}

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(pushButton_clicked(int)));

    ReadConfig();
    m_SatelliteInfo =  QGeoSatelliteInfoSource::createDefaultSource(this);
    //if (!m_SatelliteInfo) Log("Not Connected");
    connect(m_SatelliteInfo,
            SIGNAL(satellitesInUseUpdated(QList<QGeoSatelliteInfo>)),
            this,
            SLOT(satellitesInUse(QList<QGeoSatelliteInfo>)));
    connect(m_SatelliteInfo,
            SIGNAL(satellitesInViewUpdated(QList<QGeoSatelliteInfo>)),
            this,
            SLOT(satellitesInView(QList<QGeoSatelliteInfo>)));
    m_location = QGeoPositionInfoSource::createDefaultSource(this);
    connect(m_location,
            SIGNAL(positionUpdated(QGeoPositionInfo)),
            this,
            SLOT(positionUpdated(QGeoPositionInfo)));
    SetUpdateInterval();
    running = false;
    //Log("OSMapper started");
}

MainWindow::~MainWindow()
{
    m_SatelliteInfo->stopUpdates();
    m_location->stopUpdates();
    disconnect(m_location,
               SIGNAL(positionUpdated(QGeoPositionInfo)));
    disconnect(m_SatelliteInfo,
               SIGNAL(satellitesInUseUpdated(QList<QGeoSatelliteInfo>)));
    disconnect(m_SatelliteInfo,
               SIGNAL(satellitesInViewUpdated(QList<QGeoSatelliteInfo>)));
    delete ui;
}

void MainWindow::setStatusLine()
{
    QString s =
        QString("Sat (visible/used) : %1 / %2\tTrack (points/waypoints) : %3 / %4").arg(
            visibleSatellites).arg(usedSatellites).arg(track.count()).arg(
            Waypoints.count());
    ui->satellitelabel->setText(s);
}

void MainWindow::on_actionStart_mapping_triggered()
{
    //    Log("1");
    if (!running)
    {
        //      Log("2");
        usedSatellites = 0;
        visibleSatellites = 0;
        minLat = 5000;
        maxLat = 0;
        minLon = 5000;
        maxLon = 0;

        //Log("3");
        track.clear();
        Waypoints.clear();

        //Log("4");
        ui->satellitelabel->setText(tr("Wait ....."));
        //Log("4.1");
        m_SatelliteInfo->startUpdates();
        //Log("4.2");
        m_location->startUpdates();
        //Log("4.3");
        ui->actionStart_mapping->setText(tr("Stop mapping"));
        //Log("5");
    }
    else
    {
        m_SatelliteInfo->stopUpdates();
        m_location->stopUpdates();
        ui->centralWidget->setEnabled(false);
        ui->satellitelabel->setText(tr("Inactive"));
        ui->actionStart_mapping->setText(tr("Start mapping"));
        ExportTrack();
    }
    running = !running;
    //Log("6");
}

void MainWindow::satellitesInUse(QList<QGeoSatelliteInfo> slt)
{
    usedSatellites = slt.count();
    setStatusLine();
}

void MainWindow::satellitesInView(QList<QGeoSatelliteInfo> slt)
{
    visibleSatellites = slt.count();
    setStatusLine();
}

void MainWindow::positionUpdated(QGeoPositionInfo position)
{
    if (position.isValid())
    {
        if (!ui->centralWidget->isEnabled())
            ui->centralWidget->setEnabled(true);

        double la = position.coordinate().latitude();
        double lo = position.coordinate().longitude();

        if (minLat > la) minLat = la;
        if (maxLat < la) maxLat = la;


        if (minLon > lo) minLon = lo;
        if (maxLon < lo) maxLon = lo;


        track.append(new PosInfo(la, lo,
                                 position.coordinate().altitude(),
                                 position.timestamp()));
        setStatusLine();
    }
}

void MainWindow::ExportTrack()
{
    if (LastPath.isEmpty())
        LastPath = "/home/user/MyDocs/";
    QString FileName = QFileDialog::getSaveFileName(this, tr("Save Track"),
                       LastPath + QDateTime::currentDateTime().toString("yyyyMMddhhmmss") + ".gpx",
                       tr("GPX (*.gpx)"));
    if (!FileName.isEmpty())
    {
        QFileInfo finfo(FileName);
        LastPath = finfo.path() + QDir::separator();

        QFile outFile(FileName);
        if (outFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream out(&outFile);
            out.setCodec("UTF-8");
            out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                << "<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" version=\"1.1\" creator=\"OSMapper 1.2\">\n"
                << "<metadata>\n"
                << "<name>" + FormatDateTime(QDateTime::currentDateTime()) + "</name>\n"
                << "<time>" << FormatDateTime(QDateTime::currentDateTime()) << "</time>\n"
                << "<bounds minlat=\"" << QString::number(minLat,'F',10)
                << "\" minlon=\"" << QString::number(minLon,'F',10) << "\"\n"
                << "maxlat=\"" << QString::number(maxLat,'F',10)
                << "\" maxlon=\"" << QString::number(maxLon,'F',10) << "\"/>\n"
                << "</metadata>\n";

            for (int i=0; i<Waypoints.count(); i++)
            {
                out << "<wpt lat=\"" << QString::number(Waypoints[i]->Position->latitude,'F',10)
                    << "\" lon=\"" << QString::number(Waypoints[i]->Position->longitude,'F',10)
                    << "\"> <name>" << Waypoints[i]->WayPointType;
                out << "</name><time>" << Waypoints[i]->Position->DateTime << "</time></wpt>\n";
            }
            out << "<trk>\n"
                << "<name>" + FormatDateTime(QDateTime::currentDateTime()) + "</name>\n"
                << "<trkseg>\n";


            for (int i=0; i<track.count(); i++)
            {
                out << "<trkpt lat=\"" << QString::number(track[i]->latitude,'F',10)
                    << "\" lon=\"" << QString::number(track[i]->longitude,'F',10)
                    << "\"><ele>" << QString::number(track[i]->altitude,'F',2)
                    << "</ele><time>" << track[i]->DateTime
                    << "</time></trkpt>\n";
            }

            out << "</trkseg>\n"
                << "</trk>\n"
                << "</gpx>\n";

            outFile.close();
        }
    }
}

void MainWindow::ReadConfig()
{
    QSettings config(QSettings::UserScope,"OSMapper");
    int bc = config.value("Buttons/count",0).toInt();
    Buttons.clear();
    for (int i = 0; i < bc; i++)
        Buttons << config.value("Buttons/button"+QString::number(i),"").toString();
    UpdInt = config.value("GPS/UpdateInterval",5).toInt();
    Bpl = config.value("GPS/Bpl",5).toInt();
    BuildButtons();
    LastPath = config.value("File/LastPath","").toString();
}

void MainWindow::WriteConfig()
{
    QSettings config(QSettings::UserScope,"OSMapper");
    config.clear();
    config.setValue("Buttons/count",Buttons.count());
    for (int i=0; i<Buttons.count(); i++)
        config.setValue("Buttons/button"+QString::number(i),Buttons.at(i));
    config.setValue("GPS/UpdateInterval",UpdInt);
    config.setValue("GPS/Bpl",Bpl);
    config.setValue("File/LastPath",LastPath);
}
void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::information ( this, tr("OSMapper"),
                               tr("OSMapper v.1.4.1\nCopyright Kim Foder 2010\n\nHomepage : http://osmapper.garage.maemo.org/"));
}

void MainWindow::pushButton_clicked(int id)
{
    bool ok;
    QString text;
    if (id == 9999)
    {
        text = QInputDialog::getText(this, "Waypoint",
                                     tr("Enter description : "), QLineEdit::Normal,
                                     tr(""), &ok);

    }
    else
    {
        ok = true;
        text = Buttons.at(id);
    }

    if (ok && !text.isEmpty())
    {
        PosInfo* pos = track.last();  // Get last known position
        Waypoints.append(new WayInfo(pos, text));
        setStatusLine();
    }
}

void MainWindow::CommitButtons()
{
    BuildButtons();
    WriteConfig();
}

void MainWindow::on_actionNew_button_triggered()
{
    ButtonsDialog* bdlg = new ButtonsDialog();
    bdlg->SetButtons(Buttons.join("\n"));
    (bdlg->exec());
    {
        Buttons.clear();
        QString bs = bdlg->GetButtons();
        while (!bs.isEmpty())
        {
            int i = bs.indexOf("\n");
            if ((i > -1) || ((i == -1) && (!bs.isEmpty())))
            {
                if (i == -1) i = bs.length();
                QString ws = bs.left(i);
                bs.remove(0,i+1);
                if (!ws.isEmpty())
                    Buttons.append(ws);
            }
        }
        CommitButtons();
    }
    delete bdlg;
}


void MainWindow::SetUpdateInterval()
{
    int u = 5;
    if (UpdInt <= 4)
    {
        u = UpdInt+1;
    }
    else if (UpdInt <= 6)
    {
        u = (UpdInt - 3) * 5;
    }
    else u = (UpdInt - 5) * 10;

    m_location->setUpdateInterval(u * 1000);
}


void MainWindow::on_actionSample_rate_triggered()
{
    QStringList upd;
    upd << "1 sec" << "2 sec" << "3 sec" << "4 sec" << "5 sec" << "10 sec"
        << "15 sec" << "20 sec" << "30 sec" << "40 sec" << "50 sec" << "1 min";
    bool ok;
    QString item = QInputDialog::getItem(this, tr("Sample rate"),
                                         tr("Select rate:"), upd, UpdInt, false, &ok);
    if (ok && !item.isEmpty())
    {
        UpdInt = upd.indexOf(item);
        SetUpdateInterval();
        WriteConfig();
    }
}

void MainWindow::closeEvent (QCloseEvent* event )
{
    if (running)
    {
        QMessageBox msgBox;
        msgBox.setText("Tracking in progress !");
        msgBox.setInformativeText("Do you want to exit OSMapper?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Yes)
            event->accept();
        else event->ignore();
    }
    else event->accept();
}

void MainWindow::on_actionButtons_pr_line_triggered()
{
    QStringList upd;
    upd << "2" << "3" << "4" << "5" << "6" << "7";
    bool ok;
    QString item = QInputDialog::getItem(this, tr("Buttons pr. line"),
                                         tr("Buttons pr. line :"), upd, Bpl-2, false, &ok);
    if (ok && !item.isEmpty())
    {
        Bpl = upd.indexOf(item)+2;
        WriteConfig();
        BuildButtons();
    }
}

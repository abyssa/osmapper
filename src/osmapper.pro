#-------------------------------------------------
#
# Project created by QtCreator 2010-06-23T20:35:26
#
#-------------------------------------------------

TARGET   = OSMapper
#VERSION = 1.2.0
TEMPLATE = app
QT      += core gui
CONFIG  += mobility
MOBILITY = location

SOURCES += \
    main.cpp\
    MainWindow.cpp \
    ButtonsDialog.cpp

HEADERS += \
    MainWindow.h \
    ButtonsDialog.h

FORMS += \
    MainWindow.ui \
    ButtonsDialog.ui

OTHER_FILES += \
    osmapper.desktop \
    osmapper.png
